/** 
 * Information Retrieval 2016/2017
 * University of Padova
 * 
 * StemmerFinder
 * 
 * @author Federico Vendramin
 * 
 */

import java.lang.Character.Subset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class StemmerFinder {
	
	private ArrayList<String>  termList;
	private SuffixList suffixList;
	private Integer alpha;
	private float delta;
	private Integer k;
	private ArrayList<HashMap<String, KEquivalenceClass>> equivalenceMap;
	private HashMap<String, KEquivalenceClass> refinedClassMap;
	
	// Constructors 
	/**
	 * Class Constructor specifying the lexicon, suffixes and thresholds
	 * @param terms the lexicon
	 * @param suffixes the suffixes with their frequency
	 * @param alpha_param the suffix cutoff threshold
	 * @param delta_param the root cutoff threshold
	 */
	public StemmerFinder(ArrayList<String> terms, SuffixList suffixes,
								Integer alpha_param, float delta_param, Integer k_param) {
		// Initialize parameters
		alpha = alpha_param;
		delta = delta_param;
		k = k_param;
		termList = new ArrayList<String>();
		// Initialize term and suffix list
		for(int i=0; i<terms.size(); i++)
			termList.add(terms.get(i));
		suffixList = suffixes;
		
		// Initialize HashMap List
		equivalenceMap = new ArrayList<HashMap<String,KEquivalenceClass>>();
		refinedClassMap = new HashMap<String, KEquivalenceClass>();
	}
		
	public ArrayList<HashMap<String, KEquivalenceClass>> getLookupStemmer() { 
		
		KEquivalenceClass selectedClass;
		Float strength;
		Integer acceptedClassCounter = 0;
		Integer refinedClassCounter = 0;
		
		//------------- PHASE 1 ----------------->
		
		System.out.println("Finding " + k +  "-Equivalence Classes ");
		// Find all K1 equivalence classes
		findKClasses(k);

		// Scan every K class
		System.out.println("Computing classes strengths");
		for (Entry<String, KEquivalenceClass> entry : equivalenceMap.get(0).entrySet()) {
			selectedClass =  entry.getValue();
			// If the strength is above the threshold
			strength = getStrength(selectedClass.getPrefix());

			if ((strength > delta) && (selectedClass.getWordCount() > 1)) {
				//Set the class as accepted so it doesn't need refinement
				selectedClass.setStatus(true);
				acceptedClassCounter++;
				//remove all the stemmed word from the lexicon list
				for (String term : selectedClass.getTermList())
					termList.remove(term);
			} else if(selectedClass.getWordCount() > 1) {
				//REFINEMENT
				refineClass(selectedClass);
				refinedClassCounter++;
			}
				
		}
		
		System.out.println("Accepted Classes: " + acceptedClassCounter);
		System.out.println("Classes to be refined: " + refinedClassCounter);
		System.out.println("New Refined Classes: " + refinedClassMap.size());

		
		//------------- PHASE 2 ----------------->
		/*
		 * Per ogni K che va da (k1-1)>...>k2
		 * trovo le KequivalentClass (posso riutilizzare l'hashmap vecchia?)
		 * controllo getStrength se sopra la soglia --> aggiungi finalClass
		 * 
		 * NO REFINEMENT
		 */
		
		for (int i = k-1; i > 1; i--) {
			
			System.out.println("Finding " + i + "-Equivalence Classes");
			findKClasses(i);

			//Reset accepted class counter
			acceptedClassCounter = 0;
			
			//Scan every K class
			for (Entry<String, KEquivalenceClass> entry : equivalenceMap.get(this.getK1() - i).entrySet()) {
				selectedClass =  entry.getValue();
				
				strength = getStrength(selectedClass.getPrefix());
				
				// If the strength is above the threshold
				if ((strength > delta) && selectedClass.getWordCount() > 1 ) {
					//Set the class as accepted
					selectedClass.setStatus(true);
					acceptedClassCounter++;
					//remove all the stemmed word from the lexicon list
					for (String term : selectedClass.getTermList())
						termList.remove(term);
				}
			}
			
			System.out.println("Accepted Classes: " + acceptedClassCounter + " of " + equivalenceMap.get(this.getK1() - i).size());

		}

		equivalenceMap.add(refinedClassMap);
		return equivalenceMap;

	}
	
	
	// Get-Set methods
	
	/**
	 * Returns the suffix cutoff threshold 
	 * @return the suffix cutoff threshold
	 */
	public Integer getAlpha() { return alpha;	}
	
	/**
	 * Set the suffix cutoff threshold
	 * @param alpha_param the suffix cutoff threshold
	 */
	public void setAlpha(Integer alpha_param) { alpha = alpha_param; }
	
	/**
	 * Returns the root strength threshold
	 * @return the root strength threshold
	 */
	public float getDelta() { return delta; }
	
	/**
	 * Set the root strength threshold
	 * @param delta_param the root strength threshold
	 */
	public void setDelta(float delta_param) { delta = delta_param; }
	
	/**
	 * Returns the lexicon
	 * @return the lexicon
	 */
	public ArrayList<String> getTermList() { return termList; }
	
	/**
	 * Sets the lexicon
	 * @param terms the lexicon
	 */
	public void setTermList(ArrayList<String> terms) { termList = terms; } 
	
	/**
	 * Returns the list of suffixes with their frequency
	 * @return
	 */
	public SuffixList getSuffixList() { return suffixList; }
	
	/**
	 * Sets the suffix list
	 * @param suffixes
	 */
	public void setSuffixList(SuffixList suffixes) { suffixList = suffixes; } 
	
	/**
	 * Set the K1 parameter
	 * @param k_param the parameter to be set
	 */
	public void setK1(Integer k_param) { k = k_param; }
	
	/**
	 * Returns the value of K1
	 * @return The value of K1
	 */
	public Integer getK1() { return k; }
	
	
	// Private methods
	/**
	 * Finds all K-Equivalence Classes 
	 * @param k The lenght of the common prefix of the class
	 * @return The list of K-Equivalence classes found
	 */
	private void findKClasses(Integer k) { 
		
		String wordPrefix;
		String suffix;
		KEquivalenceClass selectedEquivalenceClass;
		equivalenceMap.add(new HashMap<String, KEquivalenceClass>());
		Boolean potentialFlag = false;

		//Scan each term in the list
		for (String word : termList) {
			if (word.length() < k)
				continue;
			//extract the k prefix
			wordPrefix = word.substring(0, k);
			//extract the suffix
			suffix = word.substring(k, word.length());
			// Checks if it is a potential suffix
			if ((suffix.length() != 0) && (suffixList.searchSuffix(suffix) > alpha))  // NULL is NOT potential suffix
			//if ((suffix.length() == 0) || (suffixList.searchSuffix(suffix) > alpha)) // NULL is potential suffix
				potentialFlag = true;
			else
				potentialFlag = false;

			// Checks whether the prefix maps into a KEquivalence class
			// If not, create the Kequivalence class and add the term to it
			// If yes, just add the term
			if (equivalenceMap.get(this.getK1() - k).containsKey(wordPrefix)) {
				// Select the Equivalence class
				selectedEquivalenceClass = equivalenceMap.get(this.getK1() - k).get(wordPrefix);
				// add the term
				if(!selectedEquivalenceClass.addWord(word, potentialFlag)) 
					System.out.println("Term: " + word + " NOT inserted correctly");
			} else {
				//create new equivalence class
				KEquivalenceClass newEquivalenceClass = new KEquivalenceClass(wordPrefix);
				// Add the term
				newEquivalenceClass.addWord(word, potentialFlag);
				// Put it in the HashMap
				equivalenceMap.get(this.getK1() - k).put(wordPrefix, newEquivalenceClass);
			}
		}
		
	}
	
	/**
	 * Computes the strength of the root word specified. 
	 * @param root The root used to compute the strength
	 * @return the strength of the root
	 */
	private Float getStrength(String root) {
		//Find the selected class
		HashMap<String, KEquivalenceClass> selectedHash = equivalenceMap.get(this.getK1() - root.length());
		KEquivalenceClass generatedClass = selectedHash.get(root);
		return (float)(generatedClass.getPotentialClassSize())/ generatedClass.getWordCount();
	}
	
	private void refineClass(KEquivalenceClass inputClass) {
		
		Boolean hasPotential = true;
		ArrayList<String> classTerms;
		
		while ((hasPotential) && (inputClass.getWordCount() > 0)) {
			// Order the terms of the selected C_i class
			inputClass.sortTermList();
			// Retrieve the wordlist
			classTerms = inputClass.getTermList();

			// Scan each term minus the last one of the C_i class
			for (Integer termIdx = 0; termIdx <= classTerms.size() ; termIdx++) {
				if(termIdx == classTerms.size()) {
					hasPotential = false;
					break;
				}
				
				String newPrefix = classTerms.get(termIdx);
				/*
				//MOD
				
				//Upgrade the prefix to K1+1 length
				String selectedTerm = classTerms.get(termIdx);
				String newPrefix;
				
				Integer upgradedK = inputClass.getPrefix().length();
				while ((selectedTerm.length() > inputClass.getPrefix().length()) && (selectedTerm.length() > upgradedK)) {
					newPrefix = selectedTerm.substring(0, upgradedK + 1);
	
				//ENDMOD */
				
				//Checks valid roots inside the class
				//if(getPotentialStrength(inputClass, classTerms.get(termIdx)) > delta ) {
				if(getPotentialStrength(inputClass, newPrefix) > delta ) {

					//Found a valid root
					
					ArrayList<String> subList = new ArrayList<String>();
					//String newPrefix = classTerms.get(termIdx);
					subList = selectSubclass(inputClass, newPrefix);
					// Remove the words selected from the original class
					for (String toRemove : subList)
						inputClass.removeWord(toRemove);
					
					KEquivalenceClass refinedClass = new KEquivalenceClass(newPrefix, subList);
					//mark as accepted
					if(refinedClass.getWordCount() > 1) {
						refinedClass.setStatus(true);
						//remove all the stemmed word from the lexicon list
						for (String term : refinedClass.getTermList())
							termList.remove(term);
					}
					//Insert the refined class in a new hashmap
					refinedClassMap.put(newPrefix, refinedClass);
					// Break the for loop and restart
					break;
				}
				
				/*
				upgradedK++;
				//} //MOD */
			}
			
			// Checked all the words inside the class but no potential root left
			//hasPotential = false;
		}
		
		
	}
	
	private Float getPotentialStrength(KEquivalenceClass selectedClass, String newRoot) {
		//Count how many good elements the class have
		Integer potentialSize = 0;
		Integer newGeneratedSize = 0;
		String suffix;

		for (String term : selectedClass.getTermList()) {
			if ((term.length() < newRoot.length()) || !(term.contains(newRoot) ))
				continue;
			suffix = term.substring(newRoot.length(), term.length());
			if((suffix.length() != 0) && (term.contains(newRoot)) && (newRoot.equals(term)) || ((term.length() > newRoot.length()) && (suffixList.searchSuffix(suffix) > alpha)))
				potentialSize++;
			newGeneratedSize++; 
			
		}
		//return (float)potentialSize / selectedClass.getWordCount();
		return (float)potentialSize / newGeneratedSize;
	}
	
	// Select the good terms
	private ArrayList<String> selectSubclass(KEquivalenceClass selectedClass, String newRoot) {
		ArrayList<String> toReturn = new ArrayList<String>();
		for (String term : selectedClass.getTermList()) {
			if(term.length() < newRoot.length())
				continue;
			if(term.contains(newRoot))
				toReturn.add(term);
		}
		
		return toReturn;
	}
	
}