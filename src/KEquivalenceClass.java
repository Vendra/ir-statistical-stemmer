/** 
 * Information Retrieval 2016/2017
 * University of Padova
 * 
 * KEquivalenceClass
 * 
 * From A Fast Corpus-Based Stemmer
 * JIAUL H. PAIK and SWAPAN K. PARUI, Indian
 * Statistical Institute
  * 
 * k-equivalence class: A set S = {w1, w2, ....., wn} of words is said
 * to be a k-equivalence class if ∀i, j (i = j) common-prefix-length(wi, wj) ≥ k (integer),
 * where common-prefix-length(wi, wj) maps a pair of character/symbol strings to
 * a natural number representing the number of characters/symbols present in their
 * longest common prefix. 
 * 
 * @author Federico Vendramin
 * 
 */

import java.util.ArrayList;
import java.util.Collections;

public class KEquivalenceClass {
    
    private final String commonPrefix;
    private ArrayList<String> wordList;
    private ArrayList<Boolean> potentialList;
    private boolean accepted;
    private Integer potentialSize;
    
    /**
     * Class Constructor specifying the prefix and list of member words
     * of the k-equivalence class
     * 
     * @param prefix the common prefix of the k-equivalence class
     * @param list a list of words belonging to the k-equivalence class
     */
    public KEquivalenceClass(String prefix, ArrayList<String> list) {
        
        commonPrefix = prefix;
        wordList = list;
        accepted = false;
        potentialList = new ArrayList<Boolean>();
        potentialSize = 0;
    }
    
    /**
     * Class Constructor specifying the prefix of the k-equivalence class
     * @param prefix 
     */
    public KEquivalenceClass(String prefix) {
        
        commonPrefix = prefix;
        wordList = new ArrayList<String>();
        accepted = false;
        potentialList = new ArrayList<Boolean>();
        potentialSize = 0;
    }
    
    /**
     * Add a word to the list if it has the same prefix of the class
     * @param word Word to be inserted in the k-equivalence class
     * @return True if the word is inserted correctly or is already a member
     */
    public boolean addWord(String word, Boolean potential) {
        if (this.searchWord(word))
            return true;
                
        //Check if the word to be inserted has the same prefix of the k-equivalence class
        if(commonPrefix.equals(word.substring(0, getK()))) {// getK() -1??
                    wordList.add(word);
                    potentialList.add(potential);
                    if(potential)
                    	potentialSize++;
                    return true;
                }
        return false;
                     
    }
    
    /**
     * Search a word inside the class
     * @return True if the word is already a member of the k-equivalence class
     * @param word The word to be searched inside the k-equivalence class
     */
    public boolean searchWord(String word) {
        
        if(!wordList.isEmpty()) 
            for (String wordList1 : wordList) 
                if (wordList1.equals(word)) 
                    return true;
        return false;         
    }
    
    /**
     * Remove the specified word from the k-equivalence class
     * @param word The word to be removed from the k-equivalence class
     * @return True if the word is removed correctly or is not a member
     */
    public boolean removeWord(String word) {
        if (this.searchWord(word)) {
            wordList.remove(word);
            if (potentialList.get(potentialList.size()-1) == true)
            	potentialSize--;
            potentialList.remove(potentialList.size()-1);
            return true;
        }
        return false;
    }
    
    /**
     * Returns the size K of the K-equivalence class
     * 
     * @return the size K of the equivalence class
     */
    public int getK() {
        return commonPrefix.length();
    }
    
    
    /**
     * Returns the common prefix of the equivalence class
     * 
     * @return the common prefix of the equivalence class
     */
    public String getPrefix() {
        return commonPrefix;
    }
    
    /**
     * Returns the number of words of the k-equivalence class
     * @return The number of words of the k-equivalence class
     */
    public int getWordCount() {
        return wordList.size();
    }
    
    /**
     * Returns the list of all terms contained inside the class
     * @return the list of terms contained inside the class
     */
    public ArrayList<String> getTermList() {
    	return wordList;
    }
    
    public Integer getPotentialClassSize() {
    	return potentialSize;
    }
    
    /**
     * Returns true if the strenght of the root is above the threshold
     * @return True if the strength of the root is above the threshold
     */
    public boolean isAccepted() { return accepted; }
    
    /**
     * Set the status of the class. True if the root's strength is above the threshold
     * @param state the state to be set
     */
    public void setStatus(boolean state) { accepted = state; }
    
    /**
     * Sorts the list of terms in lexicographic order
     */
    public void sortTermList() { 
    	Collections.sort(wordList);
    }
    
}