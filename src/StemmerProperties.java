import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.omg.CORBA.ExceptionList;

public class StemmerProperties {

	public void getPropertiesValue()throws IOException{
		InputStream is = null;
		try{
			Properties prop = new Properties();
			System.out.println("get file name..");
			//is = getClass().getClassLoader().getResourceAsStream(FCB.PORPERTIESNAME);
			//is = getClass().getResourceAsStream(System.getProperty("user.dir")+File.separator+"src"+File.separator+FCB.PORPERTIESNAME);
			
			
			
			try{
				is = new FileInputStream(System.getProperty("user.dir")+File.separator+"resources"+File.separator+FCB.PORPERTIESNAME);
			}catch(Exception e){
				try{
					is = new FileInputStream(System.getProperty("user.dir").substring(0,System.getProperty("user.dir").length()-4)
							+File.separator+"resources"+File.separator+FCB.PORPERTIESNAME);
				}catch(Exception ex){
					System.out.println(ex.getMessage());
				}
			}
			
			
			if(is!=null){
				prop.load(is);
			}else{
				
					throw new FileNotFoundException("properties file not found in classpath");
			}
			FCB.IDXFILEPATH = prop.getProperty("IDXFILEPATH");
			FCB.PATHOUTPUT = prop.getProperty("PATHOUTPUT");
			FCB.FOLDEROUTPUT = prop.getProperty("FOLDEROUTPUT");
			FCB.k1 = Integer.parseInt(prop.getProperty("k1"));
			FCB.k1min = Integer.parseInt(prop.getProperty("k1min"));
			FCB.k1max = Integer.parseInt(prop.getProperty("k1max"));
			FCB.delta = Double.parseDouble(prop.getProperty("delta"));
			FCB.test = Boolean.parseBoolean(prop.getProperty("test"));
			FCB.deltamin = Double.parseDouble(prop.getProperty("deltamin"));
			FCB.deltamax = Double.parseDouble(prop.getProperty("deltamax"));
			FCB.testTF = Boolean.parseBoolean(prop.getProperty("usetermfrequency"));
		}catch(Exception e){
			System.out.println("ERROR: "+e);
		}finally{
			is.close();
		}
	} 
}
