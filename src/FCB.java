/** Information Retrieval 16-17 - University of Padova
 * 
 *  FCB Main Class
 *  
 *  From A Fast Corpus-Based Stemmer
 *  JIAUL H. PAIK and SWAPAN K. PARUI, Indian
 *  Statistical Institute
 *  
 *  ACM Transactions on Asian Language Information Processing Vol. 10, No. 2,
 *  Article 8, Publication Date: June 2011
 *  
 *  @author Federico Vendramin
 *  @author Matteo Mistura
 *  
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.MessageFormat.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FCB {

	protected static String IDXFILEPATH;
	protected static final String PORPERTIESNAME="stemmer.properties";
	protected static String PATHOUTPUT;
	protected static String FOLDEROUTPUT;
	protected static int k1;
	protected static boolean test;
	protected static boolean testTF;
	protected static int k1min;
	protected static int k1max;
	protected static double delta;
	protected static double deltamin;
	protected static double deltamax;
	private static int alpha;
	private static ArrayList<HashMap<String,KEquivalenceClass>> lookup;
	public static void main(String[] args) {
		
		//System.setProperty("file.encoding", "UTF-8");
		//String test = "ячменев";
		//System.out.println(test);
		//
		try{
			new StemmerProperties().getPropertiesValue();
		}catch(Exception e){
			System.out.println("ERROR: "+e);
		}
		try{
			IDXFILEPATH = args[0];
		}catch(ArrayIndexOutOfBoundsException e){
			if(IDXFILEPATH==null){
				InputStreamReader ir = new InputStreamReader(System.in);
				BufferedReader br = new BufferedReader(ir);
				
				System.out.print("Inserisci Path dell'index: ");
				try {
					IDXFILEPATH = br.readLine();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				System.out.println("Ottimo");
			}
		}
		
		
		
		final ArrayList<String> lexicon = readLexiconFromFile(IDXFILEPATH);
		ArrayList<Word> lexiconTF=null;
		if(testTF){
			lexiconTF = readLexconAndTFFromFile(IDXFILEPATH);
		}
		System.out.println("Lexicon size:" + lexicon.size());
	
		// Find suffixes
		long start = System.currentTimeMillis(); 
		SuffixFinder sf = new SuffixFinder();
		SuffixList si = new SuffixList();
		System.out.println("Finding suffixes");

		if(testTF){
			si = (new SuffixFinder().FindSuffixesTF(lexiconTF));
		}else{
			si = sf.findSuffixes(lexicon);
		}
		
		si.sortByValues(); //ordered by Value. This Step must be done for calculate the parameter alpha
		alpha = si.getAlpha();
		System.out.println("Parameter alpha:"+alpha);
		//si.stamp();
		long elapsedTime = System.currentTimeMillis() - start;
		System.out.println("Elapsed Time: " + elapsedTime + "ms");

		/* OLD Algorithm
		// FCB Algorithm
		start = System.currentTimeMillis(); 
		System.out.println("Start FCB");
		StemmerFinder stemmer = new StemmerFinder(lexicon, si, alpha, 0.6f, 5);
		lookup = stemmer.getLookupStemmer();
		elapsedTime = System.currentTimeMillis() - start;
		System.out.println("Elapsed Time: " + elapsedTime + "ms");

		// Write Lookup Table to file
		System.out.println("Writing Lookup File");
		System.out.println("Path: " + PATHOUTPUT);
		writeStemmerToFile(PATHOUTPUT);
		System.out.println("Done!");
		*/
		
		
		if(!test) {
			StemmerFinder stemmer = new StemmerFinder(lexicon, si, alpha, (float)delta, k1);
			getStemmer(stemmer, PATHOUTPUT);
		}
		else{
			DecimalFormat df = new DecimalFormat("#.#");
			start = System.currentTimeMillis(); 
			System.out.println("!!!!!Testmode ENABLED!!!!!!");
			float deltanew = (float)deltamin;
			System.out.println("deltanew:"+deltanew+" deltamin:"+deltamin);
			int	k1new=k1min;
			ArrayList<String> copylexicon = lexicon;
			while(deltanew <= deltamax) {
				while(k1new<=k1max){
					System.out.println("\n\n----Test for delta=" + deltanew + " and k1=" + k1new + "----+\n\n");
					StemmerFinder stemmer = new StemmerFinder(copylexicon, si, alpha, deltanew, k1new);
					getStemmer(stemmer, FOLDEROUTPUT+k1new+"_" + deltanew + ".txt");
					copylexicon = lexicon;
					k1new++;
				}
				k1new=k1min;
				deltanew= (float)(deltanew+0.1);
				deltanew = (float)(Math.floor(deltanew*10))/10;
			}
			elapsedTime = System.currentTimeMillis() - start;
			System.out.println("Elapsed Time: " + elapsedTime + "ms");
		}
		
	}

	private static void getStemmer(StemmerFinder sf, String path){
		// FCB Algorithm
		long start = System.currentTimeMillis(); 
		System.out.println("Start FCB");
		lookup = sf.getLookupStemmer();
		long elapsedTime = System.currentTimeMillis() - start;
		System.out.println("Elapsed Time: " + elapsedTime + "ms");
		// Write Lookup Table to file
		System.out.println("Writing Lookup File");
		System.out.println("Path: " + path);
		writeStemmerToFile(path);
		System.out.println("Done!");
	}
	
	private static ArrayList readLexconAndTFFromFile(String path){
		return readLexiconFromFile(path, true);
	}
	private static ArrayList readLexiconFromFile(String path){
		return readLexiconFromFile(path, false);
	}
	
	private static ArrayList readLexiconFromFile(String path, boolean termFreq){
		final ArrayList lexicon = new ArrayList();
		String line=null;
		try {
			File filepath = new File(path);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filepath), "UTF-8"));
		    StringBuilder sb = new StringBuilder();
		    line = new String(br.readLine().getBytes(), Charset.forName("UTF-8"));
		    while (line != null) {
		    	sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		        if(line!=null){
		        	boolean b = true;
		        	String ww = line.split(",")[0];
		        	for(int i=0; i<ww.length(); i++){
		        		if(ww.charAt(i)>='0' && ww.charAt(i)<='9')
		        			b = false;
		        	}
		        	if(b){
		        		if(!termFreq){
		        			lexicon.add(ww);
		        		}
		        		else{
		        			Pattern pat = Pattern.compile("TF=\\d*\\s");
		        			Matcher mat = pat.matcher(line);
		        			mat.find();
		        			int frequency = Integer.parseInt(mat.group().substring(3,mat.group().length()-1));
		        			lexicon.add(new Word(ww, frequency));
		        		}
		        	}
		        }
		    }
		    br.close();
		}catch(FileNotFoundException fnfe){
			System.out.println("Path non corretto!! Inserire nuovamente:");
			InputStreamReader ir = new InputStreamReader(System.in);
			BufferedReader br1 = new BufferedReader(ir);
			try {
				String str = br1.readLine();
				return readLexiconFromFile(str);
			} catch (IOException e) {
				e.printStackTrace();	
			}
		}catch(Exception e){
			System.out.println("ERROR: "+e.toString());
		}
		return lexicon;
	}
	
	
	
	private static void writeStemmerToFile(String path){
		Writer bw = null;
		File file = null;
		try{
			file = new File(path);
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
			for(int i=0; i<lookup.size(); i++){
				HashMap<String, KEquivalenceClass> hm = lookup.get(i);
				List list = new LinkedList(hm.entrySet());
				for(Iterator it=list.iterator(); it.hasNext();){
					Map.Entry<String, KEquivalenceClass> map = (Map.Entry<String, KEquivalenceClass>) it.next();
					String stem = map.getKey();
					if(!map.getValue().isAccepted())
						continue;
					ArrayList<String> word = map.getValue().getTermList();
					for(int j=0; j<word.size(); j++){
						//bw.write(word.get(j)+"\t"+stem+"\t"+map.getValue().getK());
						bw.write(word.get(j)+"\t" + stem + "\n");
						//bw.newLine();
					}
				}
			}
			bw.close();
		}catch(Exception e){
			System.out.println("Error: "+e);
		}
	}

	
}
class Word{
	public String word;
	public int frequency;
	 public Word(String str, int freq){
		 word = str;
		 frequency = freq;
	 }

	 public int length(){
		 return word.length();
	 }
}
	
	
	
