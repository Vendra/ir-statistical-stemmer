import java.util.ArrayList;

class SuffixFinder{
	private SuffixList si;
	private int hashmap[];
	private ArrayList<int[]> words;
	public SuffixFinder(){
		si = new SuffixList();
	}
	
	//@TODO
	public SuffixList findSuffixes2(ArrayList<String> lexicon){
		char charmax = ((String)lexicon.get(0)).charAt(0);
		int sizemax = 0;
		words = new ArrayList<int[]>();
		//Preprocessing, tropo il massimo valore nell'ascii 
		System.out.print("Preprocessing...");
		for(int i=0; i<lexicon.size(); i++){
			String word = lexicon.get(i);
			
			for(int j=0; j<word.length();j++){
				if((int)charmax < (int)word.charAt(j))
					charmax = word.charAt(j);
				if(sizemax < word.length())
					sizemax = word.length();
			}
		}
		charmax++;
		System.out.println("ok");
		hashmap = new int[(int)charmax];
		System.out.println("hashmap: "+hashmap[20]);
		System.out.println("Preprocessing2");
		//Preprocessing, cambio character in numeri e comprimo
		int charidx = 1;
		for(int i=0; i<lexicon.size(); i++){
			String word = lexicon.get(i);
			int intword[] = new int[word.length()];
			//System.out.println("index:"+i+" word:"+word);
			for(int j=0; j<word.length(); j++){
				//System.out.println("letter:"+word.charAt(j)+" int:"+(int)word.charAt(j));
				//System.out.println(i+":"+"wordlenght: "+word.length()+" hashmap[word.charAt(j)]:"+hashmap[(int)word.charAt(j)]+"/"+(int)charmax+" word.charAt(j)(int):"+(int)word.charAt(j)+" charidx:"+charidx);
				if(hashmap[(int)word.charAt(j)]==0){
					hashmap[(int)word.charAt(j)] = charidx;
					charidx++;
				}
				//System.out.println("intword, i:"+i+", j:"+j+" "+intword[j]);
				intword[j] = hashmap[word.charAt(j)];
				//System.out.println("insert ok");
				
			}
			//System.out.println("Word "+i+" add:"+intword);
			words.add(intword);
			
		}
		System.out.println("OK");
		//partendo la lunghezza 1 fino alla dimensione massima che pu� avere un suffisso conto la frequenza e viene salvata in SuffixItem
		//lunghezza 1 ha un metodo diverso dagli altri
		//charidx+=2;
		int suffix[] = new int[charidx];
		for(int i=0; i<words.size(); i++){
			int[] word = words.get(i); 
			suffix[word[word.length-1]]++;
		}
		//System.out.println("Inizio salvataggio k=1 in si con lenght "+suffix.length+"/"+charidx);
		for(int i=0; i<suffix.length; i++){ //salvo tutti i suffissi di dimensione 1
			//System.out.println("Lettera:"+String.valueOf(getChar(i))+"index:"+i);
			if(suffix[i]!=0)
				si.addSuffix(String.valueOf(getChar(i)), suffix[i]);
				//System.out.println("K=1, "+String.valueOf(getChar(i))+": "+suffix[i]);
		}
		
		//con dimensione >1
		
		for(int i=2; i<sizemax-1; i++){
			int div1 = i/2 + i%2;
			int div2 = i/2;
			int frequence_table[][] = new int[(int)(Math.pow(charidx,div1))][(int)(Math.pow(charidx,div2))];
			ArrayList<int[]> position = new ArrayList<int[]>();
			String suff = "";
			//System.out.println("K:"+i);
			for(int j=0; j<words.size(); j++){
				if(words.get(j).length>i+1){
					int a=0;
					int b=0;
					//System.out.println("dimensione i:"+i+" parola j:"+j+", lunghezza:"+words.get(j).length);
					for(int t=i; t>0; t--){
						//System.out.println("suff("+t+"):"+String.valueOf(getChar(words.get(j)[words.get(j).length-1-t])));
						suff+=String.valueOf(getChar(words.get(j)[words.get(j).length-1-t]));
					}
					for(int t=0; t<div1; t++)
						a += words.get(j)[words.get(j).length-1-t]*(Math.pow(div1, t));
					for(int t=0; t<div2; t++)
						b += words.get(j)[words.get(j).length-div1-t]*(Math.pow(div2, t));
					frequence_table[a][b]++;
					if(frequence_table[a][b]==1){
						position.add(new int[] {a,b});
						//System.out.println("Position added:"+a+" "+b);
					}
				}
			}
			for(int t=0; t<position.size();t++){
				si.addSuffix(suff, frequence_table[position.get(t)[0]][position.get(t)[1]]);
				//System.out.println("K="+i+", "+suff+": "+frequence_table[position.get(t)[0]][position.get(t)[1]]);
			}
		}
		//fine algoritmo
		/*
		 * Possibili miglioramenti:
		 * 1. Ordinare le parole in base alla grandezza
		 * 2. Non fare creare frequence_table ma fare una misura ad ok
		 * */
		
		return si;
	}
	
	private char getChar(int n){
		char character='\t';
		for(int i=0; i<hashmap.length; i++){
			if(hashmap[i]==n)
				character = (char)i;
		}
		return character;
	}
	
	public SuffixList findSuffixes(ArrayList<String> lexicon){
		Node tree = new Node(null);
		for(int i=0; i<lexicon.size(); i++){
			String word = lexicon.get(i);
			Node node = tree;
			for(int j=word.length()-1; j>0; j--){
				char ch = word.charAt(j);
				if(!node.existChild(ch)){
					node.addChild(ch);
				}
				node = node.getChild(ch);
				node.addFreq();
				
			}
		}
		
		si = addSuffix(si, tree, "");
		return si;
	}
	public SuffixList FindSuffixesTF(ArrayList<Word> lexicon){
		Node tree = new Node(null);
		for(int i=0; i<lexicon.size(); i++){
			Word word = (Word)lexicon.get(i);
			Node node = tree;
			for(int j=word.length()-1; j>0; j--){
				char ch = word.word.charAt(j);
				if(!node.existChild(ch)){
					node.addChild(ch);
				}
				node = node.getChild(ch);
				node.addFreq(word.frequency);
				
			}
		}
		
		si = addSuffix(si, tree, "");
		return si;
	}
	
	private SuffixList addSuffix(SuffixList si, Node tree, String str){
		Node n = tree;	
		if(n.getChar()!=' '){
			str = n.getChar()+str;
			//System.out.println("Stringa:"+str);
			//System.out.println("Freq:"+n.getFrequency());/
			si.addSuffix(str, n.getFrequency());
		}
		//System.out.println("Added:"+str+" freq:"+n.getFrequency());
		if(n.getAllChils().size()!=0 ){
				ArrayList<Node> childs = n.getAllChils();
				for(int i=0; i<childs.size(); i++){
					si = addSuffix(si, childs.get(i), str);
				}
				return si;
		}
		return si;
	}
	
	private class Node{
		private int frequency;
		private char ch;
		private ArrayList<Node> childs;
		
		public Node(Object o){
			ch = ' ';
			frequency = 0;
			childs = new ArrayList<Node>();
		}
		public Node(char c){
			ch = c;
			frequency = 0;
			childs = new ArrayList<Node>();
		}
		public void addFreq(){
			frequency++;
		}
		public void addFreq(int n){
			frequency+=n;
		}
		
		public void addChild(char ch){
			childs.add(new Node(ch));
		}
		public boolean existChild(char c){
			for(int i=0; i<childs.size(); i++){
				if(childs.get(i).getChar()==c)
					return true;
			}
			return false;
		}
		
		public char getChar(){
			return ch;
		}
		public int getFrequency(){
			return this.frequency;
		}
		public Node getChild(char ch){
			for(int i=0; i<childs.size(); i++){
				if(childs.get(i).getChar()==ch)
					return childs.get(i);
			}
			System.out.println("ERRORE! CHILDS NON TROVATO");
			return null;
		}
		public ArrayList<Node> getAllChils(){
			return childs;
		}
	}
}