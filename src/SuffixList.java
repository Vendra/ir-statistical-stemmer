
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.nio.Buffer;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Iterator;
import java.util.AbstractList;

public class SuffixList {
	private HashMap<String, Integer> suffixMap;
	
	public SuffixList(){
		suffixMap = new HashMap<String, Integer>();
	}

	public void addSuffix(String suffix, Integer freq){
		suffixMap.put(suffix, freq);
	}

	public Integer searchSuffix(String suffix) {
		return suffixMap.get(suffix);
	}
		
	public void sortByValues(){
		HashMap hmap = suffixMap;
		List list = new LinkedList(hmap.entrySet());
		Collections.sort(list, new Comparator(){
			public int compare(Object o1, Object o2){
				return ((Comparable)((Map.Entry)(o2)).getValue()).compareTo(((Map.Entry)(o1)).getValue());
			}
		});
		
		HashMap sortedMap = new LinkedHashMap();
		for(Iterator it = list.iterator(); it.hasNext();){
			Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		suffixMap = sortedMap;
	}
	
	public int getAlpha(){//this method should contain throwed Exception 
		Iterator it = suffixMap.entrySet().iterator();
		int set1[] = new int[10];
		int set2[] = new int[10];
		int iterator1 = 0;
		int iterator2 = 0;
		for(int i=0; i<10; i++){
			//if(condition that imply there is no enough element) --> throw Exception
			set1[i]=((Map.Entry<String, Integer>)it.next()).getValue();
			//System.out.print("set1["+i+"]:"+set1[i]+" | ");
		}
		//System.out.println();
		for(int i=0; i<10; i++){
			//" "
			set2[i]=((Map.Entry<String, Integer>)it.next()).getValue();
			//System.out.print("set2["+(i+10)+"]:"+set2[i]+" | ");
		}
		double alpha = getAverage(set1);
		//System.out.println("alpha:"+alpha);
		double ratio = getAverage(set2)/getAverage(set1);
		double minRatio = ratio;
		//System.out.println("it.hasNext:"+it.hasNext()+" ratio:"+ratio);
		while(it.hasNext() && !(ratio>=0.99 && ratio<=1.01)){
			set1[iterator1]=set2[iterator2 % set2.length];
			iterator1 = (iterator1+1)%(set1.length);
			//System.out.println("iterator:"+iterator1);
			set2[iterator2]=((Map.Entry<String, Integer>)it.next()).getValue();
			iterator2 = (iterator2+1)%(set2.length);
			ratio = getAverage(set2)/getAverage(set1);
			if(minRatio < ratio){
				minRatio = ratio;
				alpha = getAverage(set1);
				//System.out.println("alpha(for-part):"+alpha);
			}
			//System.out.println("it.hasNext:"+it.hasNext()+" ratio:"+ratio);
		}
		
		return (int) Math.floor(alpha);
		
	}
	
	private double getAverage(int[] a){
		double average=0;
		for(int i=0; i<a.length; i++){
			average+=a[i];
		}
		return average/a.length;
	}
	
	public String toString(){
		String str= "";
		for(Entry<String,Integer> m: suffixMap.entrySet()){
			str+=m.getKey()+"\n";
		}
		/*
		 * for(int i=0; i<suffix.size(); i++)
			str += suffix.get(i)+";"+frequency.get(i)+"\n";
		*/
		return str;
	}
	
	public void stamp(){
		for(Entry<String,Integer> m: suffixMap.entrySet()){
			System.out.println(m.getKey());
		}
	}
}
